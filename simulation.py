import random 
import math

### Defining constants ###
i = 0
persians = i
i += 1
saxons = i 
i += 1
alamanni = i 
i += 1
nomads = i 
i += 1
goths = i
i += 1
nb_tribes = i  

region_name = {}
j = 0
britannia = j
region_name[britannia] = "britannia"
j += 1
gallia = j
region_name[gallia] = "gallia"
j += 1
hispania = j
region_name[hispania] = "hispania"
j += 1
pannonia = j
region_name[pannonia] = "pannonia"
j += 1
italia = j
region_name[italia] = "italia"
j += 1
thracia = j
region_name[thracia] = "thracia"
j += 1
macedonia = j
region_name[macedonia] = "macedonia"
j += 1
asia = j
region_name[asia] = "asia"
j += 1
pontus = j
region_name[pontus] = "pontus"
j += 1
syria = j
region_name[syria] = "syria"
j += 1
aegyptus = j
region_name[aegyptus] = "aegyptus"
j += 1
africa = j
region_name[africa] = "africa"
j += 1
nb_regions = j
### End of definitions ###

### Dictionary for binding ###
# Dictionaries containing the different invasion way
persians_spawn = {1: [pontus, asia],
    2: [syria, aegyptus]}

saxons_spawn = {1: [britannia, gallia],
        2: [gallia, hispania],
        3: [pannonia, italia]}

alamanni_spawn = {1: [pannonia, italia],
        2: [thracia, macedonia]}

nomads_spawn = {1: [africa, hispania],
        2: [aegyptus, syria]}

goths_spawn = {1: [thracia, macedonia],
    2: [asia, macedonia],
    3: [pontus, syria]}

# Dictionary containing a dictionary of invasion way per tribes
tribes_spawn = {persians: persians_spawn,
        saxons: saxons_spawn,
        alamanni: alamanni_spawn,
        nomads: nomads_spawn,
        goths: goths_spawn}
### End of block ###

def dice(nb_face = 6):
    return random.randint(1, nb_face)

def spawn():
# 2 : one from each
# 3 : persians
# 4 : saxons
# 5 : persians
# 6 : goths
# 7 : nothing
# 8 : alamanni
# 9 : nomads
# 10 : saxons
# 11 : nomads
# 12 : Nothing
    results = [0]*5
    roll = dice() + dice()
    invade = False
    if roll == 2:
        results = [1]*5
    elif roll == 3:
        results[persians] = 1
        invade = True 
    elif roll == 4:
        results[saxons] = 1
        invade = True 
    elif roll == 5:
        results[persians] = 1
        invade = True 
    elif roll == 6:
        results[goths] = 1
        invade = True 
    elif roll == 8:
        results[alamanni] = 1
        invade = True 
    elif roll == 9:
        results[nomads] = 1
        invade = True 
    elif roll == 10:
        results[saxons] = 1
        invade = True 
    elif roll == 11:
        results[nomads] = 1
        invade = True 
    
    return results, invade


# Compute wether the tribe invades or not
# Return (True, X) if it invades, (False, X) otherwise
# X equals to the number of barbarians invading
def do_arrive(tribe_state):
    # Launch the dice
    black_dice = dice()
    # If the dice is lower than the number of barbarians...
    if tribe_state >= black_dice:
        # ... they are coming!
        arrival_number = min(black_dice, tribe_state)
        return True, arrival_number
    else:
        return False, 0

# Remove the barbarians from their nest
# and place them to the invaded regions
def place_barbarians(tribe, arrival_number, region_table):
    # Launch a D2 if the tribes has two possible way of invasion
    #   "      D3           "       three         "
    # For each invaded region
    for region in tribes_spawn[tribe][dice(len(persians_spawn.keys()))]:
        # If no remaining invader, break
        if arrival_number <= 0:
            break
        # Else, compute the number of invaders for this region
        nb = min(arrival_number, 3)
        # Add it 
        region_table[region] += nb 
        # Remove this number from the remaining invaders
        arrival_number -= nb
    
# Handle for each tribe the invasion process, for one turn
def handle_arrival(tribe, tribes_table, region_table):
    arrive, arrival_number = do_arrive(tribes_table[tribe])
    if arrive:
        # Remove the barbarians from its nest
        tribes_table[tribe] -= arrival_number
        # Place the barbarians to the region
        place_barbarians(tribe, arrival_number, region_table)

# Simulate the frequency of spawn per tribes
def simu_spawn(nb_simu = 100000):
    arrival_table = [0]*nb_tribes
    for i in range(nb_simu):
        arrival_table = map(sum, zip(arrival_table, spawn()[0]))

    stats = [float(nb_simu)] * 5
    stats = map(lambda c: c[0] / c[1], zip(arrival_table, stats))
    print "results over " + str(nb_simu) + " simulations"
    print arrival_table 
    print stats

# Simulate the average number of barbarians invasions per region 
def simu_arrival(nb_simu = 10000):
    nb_turn = 60
    # Games where it arrived 
    was_invaded_table = [0]*nb_regions
    # Average barbarians per game
    avg_barbarians_table = [0]*nb_regions
    # Average invasion per game
    avg_invasion_table = [0]*nb_regions
    # For each game 
    for _ in range(nb_simu): 
        # Initialise the number of barbarians in each tribes
        tribes_table = [0]*nb_tribes
        # Will be set to 1 if it has been invaded, 0 otherwise
        was_invaded_game = [0]*nb_regions
        # For each turn
        for _ in range(nb_turn):
            region_table = [0]*nb_regions
            # Compute a spawn
            where, invade = spawn()
            # Compute the current number of barbarians in each tribes
            tribes_table = map(sum, zip(tribes_table, where)) 
            # If the spawn generates an invasion
            if invade:
                # Try to invade with each tribes
                for tribe in range(nb_tribes):
                    # If this tribe is the choosen one ...
                    if where[tribe] == 1:
                        # ... handle its invasion
                        handle_arrival(tribe, tribes_table, region_table)
            # Update avg_barbarians_table
            avg_barbarians_table = map(sum, zip(avg_barbarians_table, region_table))
            # Update avg_invasion_table 
            avg_invasion_table = map(sum, zip(avg_invasion_table, 
                map(lambda r: min(1, r), region_table))) # min(1, r) = 0 or 1
            # Update was_invaded_game
            was_invaded_game = map(sum, zip(was_invaded_game, 
                map(lambda r: min(1, r), region_table)))
            # Do not put more than one
            was_invaded_game = map(lambda r: min(r, 1), was_invaded_game)
        # Update was_invaded_table
        was_invaded_table = map(sum, zip(was_invaded_table, was_invaded_game))

    print region_table 
    for region in range(nb_regions):
        print "##########################"
        print "For region {}:".format(region_name[region])
        number = round(avg_barbarians_table[region] / float(nb_simu), 1)
        print "{} barbarians arrived per game".format(number)
        number = round(avg_invasion_table[region] / float(nb_simu), 1)
        print "Was invaded {} times per game".format(number)
        number = round(was_invaded_table[region] * 100 / float(nb_simu), 1)
        print "Was invaded {}% of game".format(number)
        print "##########################\n"

#simu_spawn()
simu_arrival()
